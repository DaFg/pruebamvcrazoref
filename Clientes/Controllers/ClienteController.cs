﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Net;
using System.Data;
using System.Data.Entity;
using Clientes;


namespace Clientes
{
	public class ClienteController : Controller
	{

		private ClientesContext db = new ClientesContext ();

		public ActionResult Index ()
		{
			var clientes = db.Clientes.ToList ();
			return View (clientes);
		}

		public ActionResult Details (int? id)
		{
			if (id == null) {

				return new HttpStatusCodeResult (HttpStatusCode.BadRequest);

			}
			Cliente clientito = db.Clientes.Find (id);
			if (clientito == null) {

				return HttpNotFound ();
			}
			return View (clientito);
		}

		public ActionResult Create ()
		{
			return View ();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create ([Bind (Include = "Nombre, Apellido, Correo, Documento")]Cliente c, FormCollection collection)
		{
			try {
				if (ModelState.IsValid) {					
					db.Clientes.Add (c);
					db.SaveChanges ();
				}
				return RedirectToAction ("Index");
			} catch {
				return View (c);
			}
		}


		public ActionResult Edit (int id)
		{
			return View ();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit ([Bind (Include = "Id, Nombre, Apellido")]Cliente c, FormCollection collection)
		{
			try {
				if (ModelState.IsValid) {
					db.Entry (c).State = EntityState.Modified;
					db.SaveChanges ();
					return RedirectToAction ("Index");
				}
			} catch {
				ModelState.AddModelError ("", "No se pueden guardar los cambios");
			}
			return View (c);
		}

		public ActionResult Delete (int id)
		{
			return View ();
		}

		[HttpPost]
		public ActionResult Delete (int id, FormCollection collection)
		{
			try {
				return RedirectToAction ("Index");
			} catch {
				return View ();
			}
		}

		protected override void Dispose (bool disposing)
		{
			db.Dispose ();
			base.Dispose (disposing);
		}


		public ActionResult Probando ()
		{
			return View ();
		}

	}
}