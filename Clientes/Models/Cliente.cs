﻿using System;
using System.Web;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Models;


namespace Clientes
{
	[Table ("Cliente", Schema = "probando")]
	public class Cliente
	{
		
		[Key]
		[DisplayName ("Numero cliente: ")]
		[Column ("Id")]
		public int Id {
			get;
			set;
		}

		[Required]
		[DisplayName ("Nombre cliente")]
		[Column ("Nombre")]
		public String Nombre {
			get;
			set;
		}

		[Required]
		[DisplayName ("Apellido cliente")]
		[Column ("Apellido")]
		public String Apellido {
			get;
			set;
		}

		[DisplayName ("Correo electronico")]
		[Column ("Correo")]
		public String Correo {
			get;
			set;
		}

		[DisplayName ("C.I./RUT")]
		[Column ("Documento")]
		public int Documento {
			get;
			set;
		}

		public virtual ICollection<Telefono> Telefonos { get; set; }

		public Cliente ()
		{
		}
	}
}

