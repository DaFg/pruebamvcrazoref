﻿using System;
using System.Web;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Clientes;

namespace Models
{
	[Table ("Telefono", Schema = "probando")]
	public class Telefono
	{
		[Key]
		[Column ("Id")]
		public int Id {
			get;
			set;
		}

		[DisplayName ("Telefono Domisilio")]
		[Column ("Domicilio")]
		public int Domicilio {
			get;
			set;
		}

		[DisplayName ("Telefono Celular")]
		[Column ("Celular")]
		public int Celular {
			get;
			set;
		}

		[DisplayName ("Telefono Trabajo")]
		[Column ("Trabajo")]
		public int Trabajo {
			get;
			set;
		}

		public int ClienteID {
			get;
			set;
		}

		public virtual ICollection<Cliente> Clientes { get; set; }

		public Telefono ()
		{
		}
	}
}

