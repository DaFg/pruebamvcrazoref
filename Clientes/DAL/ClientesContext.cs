﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Models;


namespace Clientes
{
	public class ClientesContext:DbContext
	{
		public ClientesContext () : base ("name=SoftBrainContext")
		{
		}

		public DbSet<Cliente> Clientes { get; set; }

		public DbSet<Telefono> Telefonos { get;	set; }


		protected override void OnModelCreating (DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention> ();
		}

	}
}
